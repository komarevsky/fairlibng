Betfair FairlibNG
======================

This is the Java library that lets you work with Betfair using API-NG 

[ ![Codeship Status for komarevsky/fairlibng](https://codeship.com/projects/64b45890-fd5e-0132-8760-5a06a30fe976/status?branch=master)](https://codeship.com/projects/87746)

Documentation
-------------

[http://komarevsky.bitbucket.org/fairlibng/](http://komarevsky.bitbucket.org/fairlibng/)

to contribute to documentation create pull requests for [https://bitbucket.org/komarevsky/komarevsky.bitbucket.org](https://bitbucket.org/komarevsky/komarevsky.bitbucket.org)

Build
-----

To build project type:

    gradlew clean build
    
Then take distribution from `build\libs`

To build project with integration tests:

    gradlew clean build -DappKey=HereIsYourAppKey -Dtoken=HereIsYourToken

How To use it
-------------

    // step 1 : add to your pom.xml
    <dependency>
        <groupId>org.bitbucket.komarevsky</groupId>
        <artifactId>fairlibng</artifactId>
        <version>1.0.0</version>
    </dependency>

    // step 2 : setup API
    ConnectionConfiguration connectionConfiguration = ConnectionConfiguration.INTERNATIONAL_CONFIG;
    connectionConfiguration.setApplicationKey("YOUR_APP_KEY");
    connectionConfiguration.setSessionToken("YOUR_SESSION_TOKEN");
    
    ExchangeApi exchangeApi = new ExchangeApiJsonRpc(connectionConfiguration);
    AccountsApi accountsApi = new AccountsApiJsonRpc(connectionConfiguration);
    HeartbeatApi heartbeatApi = new HeartbeatApiJsonRpc(connectionConfiguration);
    KeepAliveApi keepAliveApi = new KeepAliveApiDirect(connectionConfiguration);

    ...
    
    // step 3 : use API
    List<EventTypeResult> eventTypes = exchangeApi.listEventTypes(new MarketFilter());
    AccountFundsResponse funds = accountsApi.getAccountFunds(null);
    HeartbeatReport report = heartbeatApi.heartbeat(300);
    KeepAliveResponse alive = keepAliveApi.keepAlive();

For development
---------------

See Wiki at [https://bitbucket.org/komarevsky/fairlibng/wiki](https://bitbucket.org/komarevsky/fairlibng/wiki)

License: Apache v2
------------------

References
----------

*   The project uses code from Betfair samples on github:
    [https://github.com/betfair/API-NG-sample-code/tree/master/java/ng](https://github.com/betfair/API-NG-sample-code/tree/master/java/ng)

*   Betfair developer portal:
    [https://developer.betfair.com/](https://developer.betfair.com/)

*   API-NG documentation link:
    [https://api.developer.betfair.com/services/webapps/docs/display/1smk3cen4v3lu3yomq5qye0ni/Getting+Started+with+API-NG](https://api.developer.betfair.com/services/webapps/docs/display/1smk3cen4v3lu3yomq5qye0ni/Getting+Started+with+API-NG)
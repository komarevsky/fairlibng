package org.bitbucket.komarevsky.fairlibng.impl;

import org.bitbucket.komarevsky.fairlibng.entities.KeepAliveResponse;
import org.bitbucket.komarevsky.fairlibng.enums.KeepAliveStatus;
import org.bitbucket.komarevsky.fairlibng.testutil.BetfairAbstractIT;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class KeepAliveApiDirectIT extends BetfairAbstractIT {

    private KeepAliveApiDirect keepAliveApi;

    @Before
    public void setup() {
        keepAliveApi = new KeepAliveApiDirect(config);
    }

    @Test
    public void shouldReturnSuccess() {
        KeepAliveResponse result = keepAliveApi.keepAlive();

        assertNotNull(result);
        assertTrue(KeepAliveStatus.SUCCESS.equals(result.getStatus()));
    }
}
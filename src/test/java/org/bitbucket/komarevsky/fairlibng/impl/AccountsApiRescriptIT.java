package org.bitbucket.komarevsky.fairlibng.impl;

import org.bitbucket.komarevsky.fairlibng.entities.AccountFundsResponse;
import org.bitbucket.komarevsky.fairlibng.testutil.BetfairAbstractIT;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class AccountsApiRescriptIT extends BetfairAbstractIT {

    private AccountsApiRescript accountsApi;

    @Before
    public void setup() {
        accountsApi = new AccountsApiRescript(config);
    }

    @Test
    public void shouldReturnAccountFunds() {
        AccountFundsResponse result = accountsApi.getAccountFunds(null);

        assertNotNull(result);
    }

}
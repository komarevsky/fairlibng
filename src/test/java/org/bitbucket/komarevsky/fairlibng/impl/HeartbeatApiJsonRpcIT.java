package org.bitbucket.komarevsky.fairlibng.impl;

import org.bitbucket.komarevsky.fairlibng.entities.HeartbeatReport;
import org.bitbucket.komarevsky.fairlibng.testutil.BetfairAbstractIT;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class HeartbeatApiJsonRpcIT extends BetfairAbstractIT {

    private static final Integer MAX_HEARTBEAT = 300;

    private HeartbeatApiJsonRpc heartbeatApi;

    @Before
    public void setup() {
        heartbeatApi = new HeartbeatApiJsonRpc(config);
    }

    @Test
    public void shouldReturnHeartbeatReport() {
        HeartbeatReport report = heartbeatApi.heartbeat(MAX_HEARTBEAT);

        assertNotNull(report);
        assertTrue(report.getActualTimeoutSeconds() > 0);
    }

}
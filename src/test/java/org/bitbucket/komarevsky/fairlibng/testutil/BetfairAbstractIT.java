package org.bitbucket.komarevsky.fairlibng.testutil;

import org.bitbucket.komarevsky.fairlibng.configuration.ConnectionConfiguration;
import org.junit.Before;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.junit.Assume.assumeTrue;

/**
 * prepares integration test environment
 * if connection properties are not given then test skipped
 */
public abstract class BetfairAbstractIT {

    private static final String APP_KEY_PROP_NAME = "appKey";
    private static final String TOKEN_PROP_NAME = "token";

    protected ConnectionConfiguration config;

    @Before
    public void baseSetupBetfairAbstractIT() {
        boolean executionAllowed = isBetfairConfigAvailable();
        assumeTrue(executionAllowed);
        config = getConnectionConfiguration();
    }

    private boolean isBetfairConfigAvailable() {
        return isNotEmpty(getApplicationKey()) && isNotEmpty(getToken());
    }

    private ConnectionConfiguration getConnectionConfiguration() {
        ConnectionConfiguration config = ConnectionConfiguration.INTERNATIONAL_CONFIG;
        config.setApplicationKey(getApplicationKey());
        config.setSessionToken(getToken());
        return config;
    }

    private String getApplicationKey() {
        return System.getProperty(APP_KEY_PROP_NAME);
    }

    private String getToken() {
        return System.getProperty(TOKEN_PROP_NAME);
    }

}

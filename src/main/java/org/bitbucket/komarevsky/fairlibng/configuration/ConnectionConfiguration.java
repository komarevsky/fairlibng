package org.bitbucket.komarevsky.fairlibng.configuration;

import org.bitbucket.komarevsky.fairlibng.configuration.endpoint.AccountsEndpoint;
import org.bitbucket.komarevsky.fairlibng.configuration.endpoint.ExchangeEndpoint;
import org.bitbucket.komarevsky.fairlibng.configuration.endpoint.HeartbeatEndpoint;
import org.bitbucket.komarevsky.fairlibng.configuration.endpoint.KeepAliveEndpoint;

public class ConnectionConfiguration {

    private static final String DEFAULT_TIMEOUT_MS = "10000";

    private String exchangeApiUrl;
    private String keepAliveApiUrl;
    private String accountsApiUrl;
    private String heartbeatApiUrl;
    private String timeout = DEFAULT_TIMEOUT_MS;
    private String applicationKey;
    private String sessionToken;

    public static ConnectionConfiguration INTERNATIONAL_CONFIG = new ConnectionConfiguration() {{
        setAccountsApiUrl(AccountsEndpoint.INTERNATIONAL.getEndpoint());
        setExchangeApiUrl(ExchangeEndpoint.INTERNATIONAL.getEndpoint());
        setKeepAliveApiUrl(KeepAliveEndpoint.INTERNATIONAL.getEndpoint());
        setHeartbeatApiUrl(HeartbeatEndpoint.INTERNATIONAL.getEndpoint());
    }};

    public static ConnectionConfiguration ITALIAN_CONFIG = new ConnectionConfiguration() {{
        setAccountsApiUrl(AccountsEndpoint.INTERNATIONAL.getEndpoint());
        setExchangeApiUrl(ExchangeEndpoint.INTERNATIONAL.getEndpoint());
        setKeepAliveApiUrl(KeepAliveEndpoint.ITALIAN.getEndpoint());
        setHeartbeatApiUrl(HeartbeatEndpoint.INTERNATIONAL.getEndpoint());
    }};

    public static ConnectionConfiguration AUSTRALIAN_CONFIG = new ConnectionConfiguration() {{
        setAccountsApiUrl(AccountsEndpoint.AUSTRALIAN.getEndpoint());
        setExchangeApiUrl(ExchangeEndpoint.AUSTRALIAN.getEndpoint());
        setKeepAliveApiUrl(KeepAliveEndpoint.INTERNATIONAL.getEndpoint());
        setHeartbeatApiUrl(HeartbeatEndpoint.AUSTRALIAN.getEndpoint());
    }};

    public String getExchangeApiUrl() {
        return exchangeApiUrl;
    }

    /**
     * declared public for the case if new endpoint introduced
     * but {@link ExchangeEndpoint} not updated
     * @param exchangeApiUrl endpoint address
     */
    public void setExchangeApiUrl(String exchangeApiUrl) {
        this.exchangeApiUrl = exchangeApiUrl;
    }

    public String getKeepAliveApiUrl() {
        return keepAliveApiUrl;
    }

    /**
     * declared public for the case if new endpoint introduced
     * but {@link ExchangeEndpoint} not updated
     * @param keepAliveApiUrl endpoint address
     */
    public void setKeepAliveApiUrl(String keepAliveApiUrl) {
        this.keepAliveApiUrl = keepAliveApiUrl;
    }

    public String getAccountsApiUrl() {
        return accountsApiUrl;
    }

    /**
     * declared public for the case if new endpoint introduced
     * but {@link AccountsEndpoint} not updated
     * @param accountsApiUrl endpoint address
     */
    public void setAccountsApiUrl(String accountsApiUrl) {
        this.accountsApiUrl = accountsApiUrl;
    }

    public String getHeartbeatApiUrl() {
        return heartbeatApiUrl;
    }

    /**
     * declared public for the case if new endpoint introduced
     * but {@link AccountsEndpoint} not updated
     * @param heartbeatApiUrl endpoint address
     */
    public void setHeartbeatApiUrl(String heartbeatApiUrl) {
        this.heartbeatApiUrl = heartbeatApiUrl;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getApplicationKey() {
        return applicationKey;
    }

    public void setApplicationKey(String applicationKey) {
        this.applicationKey = applicationKey;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public ConnectionConfiguration() {}
}

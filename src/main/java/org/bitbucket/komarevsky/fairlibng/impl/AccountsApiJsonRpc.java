package org.bitbucket.komarevsky.fairlibng.impl;

import org.bitbucket.komarevsky.fairlibng.AccountsApi;
import org.bitbucket.komarevsky.fairlibng.configuration.ConnectionConfiguration;
import org.bitbucket.komarevsky.fairlibng.internal.protocol.ProtocolType;
import org.bitbucket.komarevsky.fairlibng.entities.AccountFundsResponse;
import org.bitbucket.komarevsky.fairlibng.internal.model.rpc.Container;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class AccountsApiJsonRpc extends AccountsApi {

    public AccountsApiJsonRpc(ConnectionConfiguration connectionConfiguration) {
        super(connectionConfiguration);
        protocolType = ProtocolType.JSON_RPC;
    }

    @Override
    protected Type getContainerTypeForGetAccountFunds() {
        return new TypeToken<Container<AccountFundsResponse>>() {}.getType();
    }
}

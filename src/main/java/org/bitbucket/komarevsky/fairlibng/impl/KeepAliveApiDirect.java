package org.bitbucket.komarevsky.fairlibng.impl;

import org.bitbucket.komarevsky.fairlibng.KeepAliveApi;
import org.bitbucket.komarevsky.fairlibng.configuration.ConnectionConfiguration;
import org.bitbucket.komarevsky.fairlibng.internal.protocol.ProtocolType;
import org.bitbucket.komarevsky.fairlibng.entities.KeepAliveResponse;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class KeepAliveApiDirect extends KeepAliveApi {

    public KeepAliveApiDirect(ConnectionConfiguration connectionConfiguration) {
        super(connectionConfiguration);
        protocolType = ProtocolType.DIRECT_LINK;
    }

    @Override
    protected Type getContainerTypeForKeepAlive() {
        return new TypeToken<KeepAliveResponse>() {}.getType();
    }
}

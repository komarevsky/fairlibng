package org.bitbucket.komarevsky.fairlibng.impl;

import org.bitbucket.komarevsky.fairlibng.ExchangeApi;
import org.bitbucket.komarevsky.fairlibng.configuration.ConnectionConfiguration;
import org.bitbucket.komarevsky.fairlibng.internal.protocol.ProtocolType;
import org.bitbucket.komarevsky.fairlibng.entities.EventTypeResult;
import org.bitbucket.komarevsky.fairlibng.entities.MarketBook;
import org.bitbucket.komarevsky.fairlibng.entities.MarketCatalogue;
import org.bitbucket.komarevsky.fairlibng.entities.PlaceExecutionReport;
import org.bitbucket.komarevsky.fairlibng.internal.model.rpc.Container;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class ExchangeApiJsonRpc extends ExchangeApi {

    public ExchangeApiJsonRpc(ConnectionConfiguration connectionConfiguration) {
        super(connectionConfiguration);
        protocolType = ProtocolType.JSON_RPC;
    }

    @Override
    protected Type getContainerTypeForListEventTypes() {
        return new TypeToken<Container<List<EventTypeResult>>>() {}.getType();
    }

    @Override
    protected Type getContainerTypeForListMarketBook() {
        return new TypeToken<Container<List<MarketBook>>>() {}.getType();
    }

    @Override
    protected Type getContainerTypeForListMarketCatalogue() {
        return new TypeToken<Container<List<MarketCatalogue>>>() {}.getType();
    }

    @Override
    protected Type getContainerTypeForPlaceOrders() {
        return new TypeToken<Container<PlaceExecutionReport>>() {}.getType();
    }
}

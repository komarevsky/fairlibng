package org.bitbucket.komarevsky.fairlibng.impl;

import org.bitbucket.komarevsky.fairlibng.AccountsApi;
import org.bitbucket.komarevsky.fairlibng.configuration.ConnectionConfiguration;
import org.bitbucket.komarevsky.fairlibng.internal.protocol.ProtocolType;
import org.bitbucket.komarevsky.fairlibng.entities.AccountFundsResponse;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * Documentation
 */
public class AccountsApiRescript extends AccountsApi {

    public AccountsApiRescript(ConnectionConfiguration connectionConfiguration) {
        super(connectionConfiguration);
        protocolType = ProtocolType.RESCRIPT;
    }

    @Override
    protected Type getContainerTypeForGetAccountFunds() {
        return new TypeToken<AccountFundsResponse>() {}.getType();
    }
}

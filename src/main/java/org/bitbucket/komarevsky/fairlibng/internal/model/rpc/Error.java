package org.bitbucket.komarevsky.fairlibng.internal.model.rpc;

public class Error {

	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

}

package org.bitbucket.komarevsky.fairlibng.internal.response;

import org.bitbucket.komarevsky.fairlibng.internal.protocol.ProtocolType;
import org.bitbucket.komarevsky.fairlibng.internal.json.JsonConverter;
import org.bitbucket.komarevsky.fairlibng.internal.model.rpc.Container;

import java.lang.reflect.Type;

public class ResponseConverter<T> {

    private ProtocolType protocolType;
    private String response;
    private Type containerType;

    public void setProtocolType(ProtocolType protocolType) {
        this.protocolType = protocolType;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public void setContainerType(Type containerType) {
        this.containerType = containerType;
    }

    public T convert() {
        switch (protocolType) {
            case JSON_RPC :
                return convertJsonRpc();
            case DIRECT_LINK:
            case RESCRIPT:
                return convertRescript();
            default:
                throw new UnsupportedOperationException();
        }
    }

    private T convertJsonRpc() {
        Container<T> container = JsonConverter.convertFromJson(response, containerType);
        if (container.getError() != null) {
            throw container.getError().getData().getAPINGException();
        }

        return container.getResult();
    }

    private T convertRescript() {
        return JsonConverter.convertFromJson(response, containerType);
    }
}

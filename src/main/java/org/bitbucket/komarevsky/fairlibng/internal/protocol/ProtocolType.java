package org.bitbucket.komarevsky.fairlibng.internal.protocol;

public enum  ProtocolType {
    JSON_RPC, RESCRIPT, DIRECT_LINK;
}

package org.bitbucket.komarevsky.fairlibng.internal.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.lang.reflect.Type;
import java.util.Date;

/**
 * Useful class to convert to and from Json
 * In this example we use Google gson
 */
public class JsonConverter {

    /**
     * We needed to override the adapter for the Date class as Betfair's API-NG requires all dates to be serialized in ISO8601 UTC
     * Just formatting the string to the ISO format does not adjust by the timezone on the Date instance during serialization.
     */
    private static final Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new ISO8601DateTypeAdapter()).create();

    /**
     * This method deserializes the specified Json into an object of the specified class.
     * @param toConvert json string that must be converted to object
     * @param clazz class of resulting object
     * @param <T> type of resulting object
     * @return object of the specified class
     */
    public static  <T> T convertFromJson(String toConvert,  Class<T>  clazz){
        return gson.fromJson(toConvert, clazz);
    }

    /**
     * This method deserializes the specified Json into an object of the specified Type.
     * @param toConvert json string that must be converted to object
     * @param typeOfT holder of resulting type
     * @param <T> type of resulting object
     * @return object of the specified Type
     */
    public static  <T> T convertFromJson(String toConvert,  Type  typeOfT){
        return gson.fromJson(toConvert, typeOfT);
    }

    /**
     * This method serializes the specified object into its equivalent Json representation.
     * @param toConvert object to convert to json
     * @return json string
     */
    public static String convertToJson(Object toConvert){
        return gson.toJson(toConvert);

    }
}

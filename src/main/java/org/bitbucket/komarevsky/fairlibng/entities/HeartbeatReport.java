package org.bitbucket.komarevsky.fairlibng.entities;

import org.bitbucket.komarevsky.fairlibng.enums.ActionPerformed;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;

/**
 * Response from heartbeat operation
 * see https://api.developer.betfair.com/services/webapps/docs/display/1smk3cen4v3lu3yomq5qye0ni/Heartbeat+API#HeartbeatAPI-HeartbeatReport
 */
public class HeartbeatReport {

    private ActionPerformed actionPerformed;
    private Integer actualTimeoutSeconds;

    public ActionPerformed getActionPerformed() {
        return actionPerformed;
    }

    public void setActionPerformed(ActionPerformed actionPerformed) {
        this.actionPerformed = actionPerformed;
    }

    public Integer getActualTimeoutSeconds() {
        return actualTimeoutSeconds;
    }

    public void setActualTimeoutSeconds(Integer actualTimeoutSeconds) {
        this.actualTimeoutSeconds = actualTimeoutSeconds;
    }

    @Override
    public int hashCode() {
        return reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return reflectionToString(this);
    }
}

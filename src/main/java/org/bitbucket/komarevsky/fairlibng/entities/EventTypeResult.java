package org.bitbucket.komarevsky.fairlibng.entities;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;

public class EventTypeResult {
	private EventType eventType ; 
	private int marketCount;
	
	public EventType getEventType() {
		return eventType;
	}

    public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

    public int getMarketCount() {
		return marketCount;
	}

    public void setMarketCount(int marketCount) {
		this.marketCount = marketCount;
	}

	@Override
	public int hashCode() {
		return reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return reflectionToString(this);
	}
}

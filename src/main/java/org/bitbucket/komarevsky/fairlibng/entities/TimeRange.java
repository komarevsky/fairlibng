package org.bitbucket.komarevsky.fairlibng.entities;

import java.util.Date;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;

public class TimeRange {

	private Date from;

	public final Date getFrom() {
		return from;
	}

	public final void setFrom(Date from) {
		this.from = from;
	}

	private Date to;

	public final Date getTo() {
		return to;
	}

	public final void setTo(Date to) {
		this.to = to;
	}

	@Override
	public int hashCode() {
		return reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return reflectionToString(this);
	}
}

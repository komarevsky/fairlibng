package org.bitbucket.komarevsky.fairlibng.entities;

import org.bitbucket.komarevsky.fairlibng.enums.InstructionReportErrorCode;
import org.bitbucket.komarevsky.fairlibng.enums.InstructionReportStatus;

import java.util.Date;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;

public class PlaceInstructionReport {

	private InstructionReportStatus status;
	private InstructionReportErrorCode errorCode;
	private PlaceInstruction instructionl;
	private String betId;
	private Date placedDate;
	private Double averagePriceMatched;
	private Double sizeMatched;

	public InstructionReportStatus getStatus() {
		return status;
	}

	public void setStatus(InstructionReportStatus status) {
		this.status = status;
	}

	public InstructionReportErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(InstructionReportErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public PlaceInstruction getInstructionl() {
		return instructionl;
	}

	public void setInstructionl(PlaceInstruction instructionl) {
		this.instructionl = instructionl;
	}

	public String getBetId() {
		return betId;
	}

	public void setBetId(String betId) {
		this.betId = betId;
	}

	public Date getPlacedDate() {
		return placedDate;
	}

	public void setPlacedDate(Date placedDate) {
		this.placedDate = placedDate;
	}

	public Double getAveragePriceMatched() {
		return averagePriceMatched;
	}

	public void setAveragePriceMatched(Double averagePriceMatched) {
		this.averagePriceMatched = averagePriceMatched;
	}

	public Double getSizeMatched() {
		return sizeMatched;
	}

	public void setSizeMatched(Double sizeMatched) {
		this.sizeMatched = sizeMatched;
	}

    @Override
    public int hashCode() {
        return reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return reflectionToString(this);
    }
}

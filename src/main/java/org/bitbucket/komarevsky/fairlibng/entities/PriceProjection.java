package org.bitbucket.komarevsky.fairlibng.entities;

import org.bitbucket.komarevsky.fairlibng.enums.PriceData;

import java.util.Set;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;


public class PriceProjection {

	private Set<PriceData> priceData;
	private ExBestOfferOverRides exBestOfferOverRides;
	private boolean virtualise;
	private boolean rolloverStakes;

	public Set<PriceData> getPriceData() {
		return priceData;
	}

	public void setPriceData(Set<PriceData> priceData) {
		this.priceData = priceData;
	}

	public ExBestOfferOverRides getExBestOfferOverRides() {
		return exBestOfferOverRides;
	}

	public void setExBestOfferOverRides(
			ExBestOfferOverRides exBestOfferOverRides) {
		this.exBestOfferOverRides = exBestOfferOverRides;
	}

	public boolean isVirtualise() {
		return virtualise;
	}

	public void setVirtualise(boolean virtualise) {
		this.virtualise = virtualise;
	}

	public boolean isRolloverStakes() {
		return rolloverStakes;
	}

	public void setRolloverStakes(boolean rolloverStakes) {
		this.rolloverStakes = rolloverStakes;
	}

    @Override
    public int hashCode() {
        return reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return reflectionToString(this);
    }

}

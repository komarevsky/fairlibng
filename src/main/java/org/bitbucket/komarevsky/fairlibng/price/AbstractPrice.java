package org.bitbucket.komarevsky.fairlibng.price;

import org.bitbucket.komarevsky.fairlibng.internal.model.PriceRule;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.List;

/**
 * common implementation for classes that must correct price increments
 */
public abstract class AbstractPrice {

    private double price;

    protected static List<PriceRule> getPriceRuleListByRulesArray(double[][] betfairRules) {
        List<PriceRule> ruleList = new LinkedList<>();

        for (double[] rule : betfairRules) {
            double from = rule[0];
            double to = rule[1];
            double increment = rule[2];

            for (double d=from; d<to; ) {
                BigDecimal bdTo = new BigDecimal(d+increment).setScale(2, RoundingMode.HALF_UP);
                ruleList.add(new PriceRule(d, bdTo.doubleValue(), increment));
                d = bdTo.doubleValue();
            }
        }

        return ruleList;
    }

    public abstract double getMaxPrice();

    public abstract double getMinPrice();

    protected abstract List<PriceRule> getPriceRuleList();

    public AbstractPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isValid() {
        if (price < getMinPrice() || price > getMaxPrice()) {
            return false;
        }

        for (PriceRule r : getPriceRuleList()) {
            if (price == r.getFrom() || price == r.getTo()) {
                return true;
            }
        }

        return false;
    }

    public OddsMarketPrice getNextPrice() {
        if (price < getMinPrice()) {
            return new OddsMarketPrice(getMinPrice());
        } else if (price >= getMaxPrice()) {
            return new OddsMarketPrice(getMaxPrice());
        }

        for (PriceRule r : getPriceRuleList()) {
            double from = r.getFrom();
            double to = r.getTo();
            double increment = r.getIncrement();

            if (price >= from && price < to) {
                BigDecimal newPriceValue = new BigDecimal(from + increment).setScale(2, RoundingMode.HALF_UP);
                return new OddsMarketPrice(newPriceValue.doubleValue());
            }
        }

        return null;
    }

    public OddsMarketPrice getPreviousPrice() {
        if (price <= getMinPrice()) {
            return new OddsMarketPrice(getMinPrice());
        } else if (price > getMaxPrice()) {
            return new OddsMarketPrice(getMaxPrice());
        }

        for (PriceRule r : getPriceRuleList()) {
            double from = r.getFrom();
            double to = r.getTo();
            double increment = r.getIncrement();

            if (price > from && price <= to) {
                BigDecimal newPriceValue = new BigDecimal(to - increment).setScale(2, RoundingMode.HALF_DOWN);
                return new OddsMarketPrice(newPriceValue.doubleValue());
            }
        }

        return null;
    }

}

package org.bitbucket.komarevsky.fairlibng.price;

import org.bitbucket.komarevsky.fairlibng.internal.model.PriceRule;

import java.util.List;

/**
 * Class to work with price according to betfair price increments
 */
public class OddsMarketPrice extends AbstractPrice {

    public static final double MIN_PRICE = 1.01;
    public static final double MAX_PRICE = 1000;

    // list of rules for every possible interval
    private static final List<PriceRule> ruleList;

    //initialization of ruleList
    static {
        // array of [FROM_PRICE, TO_PRICE, INCREMENT]
        double[][] betfairRules = {
                {MIN_PRICE, 2, 0.01},
                {2, 3, 0.02},
                {3, 4, 0.05},
                {4, 6, 0.1},
                {6, 10, 0.2},
                {10, 20, 0.5},
                {20, 30, 1},
                {30, 50, 2},
                {50, 100, 5},
                {100, MAX_PRICE, 10}
        };

        ruleList = getPriceRuleListByRulesArray(betfairRules);
    }

    public OddsMarketPrice(double price) {
        super(price);
    }

    @Override
    public double getMaxPrice() {
        return MAX_PRICE;
    }

    @Override
    public double getMinPrice() {
        return MIN_PRICE;
    }

    @Override
    protected List<PriceRule> getPriceRuleList() {
        return ruleList;
    }


}

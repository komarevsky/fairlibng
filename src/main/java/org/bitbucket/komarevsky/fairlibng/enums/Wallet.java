package org.bitbucket.komarevsky.fairlibng.enums;

/**
 * see https://api.developer.betfair.com/services/webapps/docs/display/1smk3cen4v3lu3yomq5qye0ni/Accounts+Enums#AccountsEnums-Wallet
 */
public enum  Wallet {
    UK,
    AUSTRALIAN
}

package org.bitbucket.komarevsky.fairlibng.enums;

public enum ExecutionReportStatus {
	SUCCESS, FAILURE, PROCESSED_WITH_ERRORS, TIMEOUT;
}

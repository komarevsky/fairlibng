package org.bitbucket.komarevsky.fairlibng.enums;

public enum TimeGranularity {
	DAYS, HOURS, MINUTES;

}

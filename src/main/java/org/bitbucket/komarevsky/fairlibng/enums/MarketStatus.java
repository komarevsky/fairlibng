package org.bitbucket.komarevsky.fairlibng.enums;

public enum MarketStatus {
    INACTIVE,
    OPEN,
    SUSPENDED,
    CLOSED
}

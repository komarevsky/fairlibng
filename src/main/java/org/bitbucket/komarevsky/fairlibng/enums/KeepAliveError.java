package org.bitbucket.komarevsky.fairlibng.enums;

public enum  KeepAliveError {
    INPUT_VALIDATION_ERROR,
    INTERNAL_ERROR,
    NO_SESSION
}

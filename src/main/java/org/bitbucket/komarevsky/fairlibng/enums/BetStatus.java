package org.bitbucket.komarevsky.fairlibng.enums;

public enum BetStatus {
    SETTLED,
    VOIDED,
    LAPSED,
    CANCELLED
}

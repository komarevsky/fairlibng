package org.bitbucket.komarevsky.fairlibng.enums;

public enum RollupModel {
	STAKE, PAYOUT, MANAGED_LIABILITY, NONE;
}

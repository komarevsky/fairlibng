package org.bitbucket.komarevsky.fairlibng.enums;

public enum OrderStatus {
	EXECUTION_COMPLETE, EXECUTABLE;
}

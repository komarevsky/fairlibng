package org.bitbucket.komarevsky.fairlibng.enums;

public enum GroupBy {
    EVENT_TYPE,
    EVENT,
    MARKET,
    SIDE,
    BET
}

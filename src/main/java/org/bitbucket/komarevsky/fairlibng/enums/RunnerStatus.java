package org.bitbucket.komarevsky.fairlibng.enums;

public enum RunnerStatus {
    ACTIVE,
    WINNER,
    LOSER,
    REMOVED_VACANT,
    REMOVED,
    HIDDEN
}

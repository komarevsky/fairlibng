package org.bitbucket.komarevsky.fairlibng.enums;

public enum OrderType {
	LIMIT, LIMIT_ON_CLOSE, MARKET_ON_CLOSE;
}
